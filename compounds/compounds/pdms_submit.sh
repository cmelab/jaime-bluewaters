#!/bin/bash -l
#SBATCH -p batch
#SBATCH -J silox
#SBATCH -o silox.log
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mail-type=All
#SBATCH --mail-user=jaimeguevara@u.boisestate.edu
#SBATCH -t 48:00:00
#SBATCH --gres=gpu:1

cd /home/jaimeguevara/Projects/jaime-bluewaters/compounds
module load cuda80/fft/8.0.61
python 80pdms-scaled.py
