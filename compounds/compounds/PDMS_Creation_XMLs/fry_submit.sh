#!/bin/bash -l
#SBATCH -p batch
#SBATCH -J silox_rerun
#SBATCH -o silox_pdms_rerun.log
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mail-type=All
#SBATCH --mail-user=jaimeguevara@u.boisestate.edu
#SBATCH -t 48:00:00
#SBATCH --gres=gpu:1

cd /home/jaimeguevara/Projects/jaime-bluewaters/compounds/PDMS_Creation_XMLs
module load cuda80/fft/8.0.61
python Initialized_PDMS.py
