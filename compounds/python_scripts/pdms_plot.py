import numpy as np
from matplotlib import pyplot 

# Input filename
fileName = raw_input('Enter filename: ')

# Column to use vs time
col = raw_input('Enter column to plot vs time: ')

data = np.genfromtxt(fname=fileName);  #,skip_header=True);
pyplot.figure(figsize=(4,2.2), dpi=140);
pyplot.plot(data[1:,0], data[1:,int(col)]);
pyplot.xlabel(data[0,int(col)]);
pyplot.ylabel('time steps');
pyplot.show();
