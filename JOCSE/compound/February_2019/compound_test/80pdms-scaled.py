import hoomd
import hoomd.md
import hoomd.deprecated
import numpy as np

# Original epsilon and sigma by which all values will be scaled
epsilon = 0.585 # kcal/mol
sigma = 3.786 # Angstroms

kBond = epsilon/(sigma**2)
# kDihedral = null;

def setPairCoeffs():
    # ---=== Pair Potentials ===---

    # Create the initial neighbour list
    nl = hoomd.md.nlist.cell()

    # Initialise a Lennard Jones pair potential
    lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)
    # Set the epislon and sigma values for each atom type
    # that is present in the system first.
    # Values are modified from OPLS-UA/AMBER
    # Epsilon units orginally are in kcal/mol they have been modified to units of energy/units of distance
    # Sigma units were originally in Angstroms and are now just distance units
    pairData = {'CH3': [0.3077, 1],
                'Si': [1, 0.8941],
                'O': [0.3468, 0.7805]}
    # Pairs are determined by geometric average of epsilon
    # and sigma
    for atom1, coeffs1 in pairData.items():
        for atom2, coeffs2 in pairData.items():
            lj.pair_coeff.set(atom1, atom2,
                epsilon=np.sqrt(coeffs1[0] * coeffs2[0]),
                sigma=np.sqrt(coeffs1[1] * coeffs2[1]))

def setBondCoeffs():
    # ---=== Bond Potentials ===---
    # Initialise a harmonic bond
    harmonicBond = hoomd.md.bond.harmonic()
    # Call in our new k-scaler and distance-scaler
    global kBond
    global sigma
    # Set the k and r0 values for each bond
    # Values are from the hybrid/UA force field of Frischknecht and Curro
    harmonicBond.bond_coeff.set('O-Si', k=(700.24/kBond), r0=(1.64/sigma))
    harmonicBond.bond_coeff.set('CH3-Si', k=(379.3/kBond), r0=(1.90/sigma))
    # It would seem, according to Frischknecht and Curro that indeed we'd only worry about Si-O and Si-CH3 bonds

def setAngleCoeffs():
    # ---=== Angle Potentials ===---
    # Initialise a harmonic angle
    harmonicAngle = hoomd.md.angle.harmonic()
    # Call for epsilon in order to scale our angle k, this is because radians don't change
    global epsilon
    # Set the k and t0 values for each angle
    # Values are modified from the hybrid/UA force field of Frischknecht and Curro
    harmonicAngle.angle_coeff.set('Si-O-Si', k=(28.28/epsilon), t0=2.56)
    harmonicAngle.angle_coeff.set('O-Si-O', k=(189.0/epsilon), t0=1.88) 
    harmonicAngle.angle_coeff.set('CH3-Si-CH3', k=(99.94/epsilon), t0=1.91) 
    harmonicAngle.angle_coeff.set('CH3-Si-O', k=(99.94/epsilon), t0=1.93) 
    # k values were modified from kcal/mol rad**2 to energy units/radians**2 to fit the HOOMD format

# def multiHarmonicTorsion(theta, V0, V1, V2):
    # Definition of multiharmonic dihedral equation based on 3 input parameters to be used by HOOMD
    # The equation can be written as: V = \sum_{i = 0}^{4} V_{i} cos^{i}(\theta)
    # V = V0 + V1 * (1+np.cos(theta)) + V2 * (1+(np.cos(theta))**2) # + V3 * (1+(np.cos(theta))*3) + V4 * (1+(np.cos(theta))*4)
    # F = V1 * np.sin(theta) + 2 * V2 * np.cos(theta) * np.sin(theta) # + 3 * V3 * ((np.cos(theta))**2) * np.sin(theta) + 4 * V4 * ((np.cos(theta))**3) * np.sin(theta)
    # F is the negative first derivative of V
    # return (V)


def setDihedralCoeffs():
    # ---=== Dihedral Potentials ===---
    # Initialise a tabulated dihedral
    harmonicDihedral = hoomd.md.dihedral.harmonic()
    # Call in our energy scalar since k is in energy units
    global epsilon
    # Set the k, d, and n values for each bond
    # d will be assumed to be 1 and -1 for all intents and purposes 
    # Values are modified from the hybrid/UA force field of Frischknecht and Curro
    harmonicDihedral.dihedral_coeff.set('O-Si-O-Si', k=(0.45/epsilon), d=1, n=1)
    harmonicDihedral.dihedral_coeff.set('CH3-Si-O-Si', k=(0.02/epsilon), d=-1, n=3)

if __name__ == "__main__":
    # Start with the name of the file to be run
    fileName = './PDMS_sealant_modified.xml'
    
    # Ask for new gsd & xml filenames
    #OPname = raw_input('Enter output filename: ')

    # Initialise the system
    hoomd.context.initialize("")
    system = hoomd.deprecated.init.read_xml(filename = fileName)

    # Set the coefficients
    setPairCoeffs()
    setBondCoeffs()
    setAngleCoeffs()
    setDihedralCoeffs()

    # Set the groups
    all = hoomd.group.all()

    # Create the integrator
    hoomd.md.integrate.mode_standard(dt=1e-3)
    integrator = hoomd.md.integrate.nvt(group=all, tau=2, kT=0.584)

    # Set the total runtime
    runTime = 1e8

    # Tell HOOMD that we want our box to be updated in order to better use our normalized parameters
    # hoomd.update.box_resize(L=hoomd.variant.linear_interp([(0,200), (1e7,200/sigma)]), scale_particles=True)

    # Set the output files
    # hoomd.deprecated.dump.xml(group=all, filename="11-20-17_scaledMorph_8020pdms.xml", vis=True)
    hoomd.dump.gsd(group=all, filename=fileName.replace(".xml", "_kt0.584_1e-3.gsd"), period=int(runTime/100), overwrite=True)
    hoomd.analyze.log(filename=fileName.replace(".xml","_kt0.584_1e-3.log"), quantities=['temperature', 'pair_lj_energy', 'bond_harmonic_energy', 'angle_harmonic_energy', 
                                                        'dihedral_harmonic_energy', 'potential_energy'], period=100, header_prefix='#', overwrite=True)
    hoomd.run(runTime)
